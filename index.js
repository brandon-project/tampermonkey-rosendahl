// ==UserScript==
// @name         BrandOn prices parser
// @namespace    http://tampermonkey.net/
// @version      0.40
// @description  Parse products price and export as CSV to be imported in BrandOn
// @author       ipx.work@gmail.com
// @connect      b2b-api.rdg.dk
// @match        https://sales.rdg.dk/vishovedkategorier.asp
// @match        https://portal.rdg.dk/sales/products*
// @match        https://b2b.fh-as.dk/produkter*
// @icon         https://sales.rdg.dk/favicon.ico
// @grant        GM_webRequest
// @grant        GM_xmlhttpRequest
// @grant        unsafeWindow
// @run-at       document-start
// @require      http://code.jquery.com/jquery-latest.js
// @downloadURL  https://gitlab.com/brandon-project/tampermonkey-rosendahl/-/raw/main/index.js
// @updateURL    https://gitlab.com/brandon-project/tampermonkey-rosendahl/-/raw/main/index.js
// ==/UserScript==

(function() {
    'use strict';

    const rowColumns = {
        'RS': {
            sku: 'SKU',
            category: 'Brand',
            name: 'Name',
            price: 'Price',
            campaign: 'Campaign',
            discount: 'Discount %',
        },
        'FH': {
            sku: 'SKU',
            category: 'Brand',
            name: 'Name',
            price: 'Recommended Price',
            net_price: 'NET Price',
            campaign_price: 'Campaign Price',
            campaign: 'Campaign',
        },
        'RP': {
            sku: "SKU",
            category: "Brand",
            name: "Name",
            price: "Recommended Price",
            net_price: "NET Price",
            gross_price: "Gross Price",
            discount: "Discount %",
            campaign_price: "Campaign Price",
            ean: "EAN",
            diameter: "Diameter",
            volume: "Volume",
            width: "Width",
            height: "Height",
            depth: "Depth",
            weight: "Weight",
            boxDepth: "Box Depth",
            boxHeight: "Box Height",
            boxWidth: "Box Width",
        }
    }

    const FILE_PREFIX = {
        'FH': 'F&H',
        'RS': 'Rosendahl_sales',
        'RP': 'Rosendahl_portal',
    }
    const PAGE_SIZE = 100

    const request = (url, {method = 'get', data, headers = {}} = {}) => new Promise((resolve, reject) => {
        GM_xmlhttpRequest ( {
            method,
            url,
            headers:    {
                "Content-Type": "application/json",
                ...headers
            },
            data,
            onload:     function (response) {
                resolve(JSON.parse(response.responseText))
            },
            onerror:    function(response) {
                console.error("error: ", response);
                reject(response)
            }
        } );
    })

    const startParse = (type) => () => {
        button.text(`Collecting products...`)
        button.attr('disabled', true)

        const parserFn = {
            'FH': parseFH,
            'RS': parseRS,
            'RP': parseRP
        }

        parserFn[type]().then(rows => {
            const href = exportLink(rows)
            const fileName = `${FILE_PREFIX[type]}-${(new Date()).toISOString().substr(0, 10)}.csv`

            button.text(`Download: ${fileName}`);

            button.attr({
                href,
                download: fileName,
                disabled: false
            })
        }).catch(err => {
            console.log('cant collecting', err)

            button.text('Update prices')
            button.attr({
                href: '#',
                disabled: false
            })
            button.on('click', buttonAction)
        })
    }

    const parserNumber = (number) => Number(number.replace('.', '').replace(',', '.'))

    const exportLink = (rows) => {
        let csvContent = rows.map(row => row.map(cell => {
            if (cell === null || cell === undefined) {
                cell = ''
            } else if (String(cell).includes(',') || String(cell).includes('#')) {
                cell = `"${String(cell).replace(';', '\;').replace('#', '\\#')}"`
            }
            return String(cell).replace('\n', '')
        }).join(';')).join("\n")

        return encodeURI(`data:text/csv;base64,${btoa(unescape(encodeURIComponent(csvContent)))}`)
    }

    const formatRow = (type, product, options) => {
        let formatRow = null
        const {brands} = options

        switch (type) {
            case 'FH':
                const brand = brands.filter(brand => product.name.includes(brand)).sort((a, b) => b.length - a.length).shift()
                const campaignPrice = product.hasCampaign && parserNumber(product.campaignPrice.replace(/.*e-price-price">([\d,.]+)<\/.*/g, '$1'))

                formatRow = {
                    sku: product.number,
                    category: brand,
                    name: brand ? product.name.substr(brand.length).trim() : product.name,
                    campaign: product.isInCampaigns.map(campain => campain.campaignName).join(', '),
                    campaign_price: campaignPrice || '',
                    price: product.productRetailSalesPrice,
                    net_price: product.priceClean,
                }
                break;
            case 'RP':
                const price = product.priceDtos.length && product.priceDtos[0]

                if (!price) break;

                formatRow = {
                    sku: product.productNo,
                    category: brands[product.brandNo],
                    name: product.description,
                    campaign_price: parserNumber(price.campaignPrice) || '',
                    net_price: parserNumber(price.netPrice),
                    price: product.recommendedRetailPrice,
                    gross_price: price.grossPrice,
                    discount: price.discountPercentage,

                    ean: product.ean,

                    diameter: parserNumber(product.dimension.diameterCm) || '',
                    volume: parserNumber(product.dimension.volumeL) || '',
                    width: parserNumber(product.dimension.widthCm) || '',
                    height: parserNumber(product.dimension.heightCm) || '',
                    depth: parserNumber(product.dimension.depthCm) || '',
                    weight: parserNumber(product.dimension.grossWeightKg) || '',

                    boxDepth: parserNumber(product.brownBox.depthCm) || '',
                    boxHeight: parserNumber(product.brownBox.heightCm) || '',
                    boxWidth: parserNumber(product.brownBox.widthCm) || '',
                }
                break;
            case 'RS':
                formatRow = {
                    sku: product.itemNumber,
                    category: brands[product.brandID],
                    name: product.itemText,
                    campaign: product.campaignID,
                    discount: product.discountPercent,
                    net_price: product.price,
                }
                break;
        }

        return formatRow && Object.keys(rowColumns[type]).map(key => formatRow[key] || '');
    }

    const productsToFile = (type, products, brands) => {
        if (!type || !products.length) return null

        const rows = products.map(product => formatRow(type, product, {brands})).filter(Boolean)

        rows.unshift(Object.values(rowColumns[type]))

        return rows
    }

    const parseRS = async () => {
        const brands = $('.tablinks').not('#menu_Campaign').toArray()
            .map(elm => ({
                id: elm.id.replace('menu_', ''),
                name: $(elm).children(':first-child').attr('title')
            }))
            .reduce((obj, elm) => ({...obj, [elm.id]: elm.name}), {})
        const products = []

        for (let cat in brands) {
            const catProducts = await request(`https://sales.rdg.dk/merchant.asp?getItemList=true&qsMainCategoryName=${cat}&qsSrc=&qsOffset_a=0&qsOffset_b=999999`)
            products.push(...catProducts)
        }

        return productsToFile('RS', products, brands)
    }

    const parseFH = async () => {
        const params = new URLSearchParams(window.location.search)

        params.set('PageSize', String(PAGE_SIZE))

        const {products = [], facets = [], page = {}} = {page: {pageNum: 0, totalPages: Infinity}}

        while (page.pageNum < page.totalPages) {
            console.log(`Request ${page.pageNum + 1}/${page.totalPages}`)

            buttonProgress(page.pageNum / page.totalPages)

            params.set('PageNum', String(page.pageNum + 1))

            const {products: products_part = [], facets: facets_part = [], page: page_part = {}} = await request(`https://b2b.fh-as.dk/system/data/facets?${params}`)
                .catch(() => {
                    return {products: [], facets: []}
                })
            page.pageNum = page_part.pageNum
            page.totalPages = page_part.totalPages

            products.push(...products_part)
            facets.push(...facets_part)
        }


        const brands = facets.find(facet => facet.name === 'Brand').options.filter(option => option.count || !option.isDisabled).map(option => option.label)

        return productsToFile('FH', products, brands)
    }

    const parseRP = async () => {
        const {products = [], facets = [], page={}} = {page: {pageNumber: 0, pagesInAll: Infinity}}

        while (page.pageNumber < page.pagesInAll) {
            const nextPage = page.pageNumber + 1
            console.log(`Request ${nextPage}/${page.pagesInAll}`)

            buttonProgress(page.pageNumber / page.pagesInAll)

            const {
                products: products_part = [],
                facets: facets_part = [],
                pageNumber,
                pagesInAll
            } = await request(`https://b2b-api.rdg.dk/api/product/search`, {
                method: 'post',
                data: JSON.stringify({
                    ...requestFilters,
                    pageSize: PAGE_SIZE,
                    pageNumber: nextPage
                }),
                headers: {authorization: requestToken}
            }).catch(() => {
                return {products: [], facets: []}
            })

            page.pageNumber = pageNumber
            page.pagesInAll = pagesInAll

            products.push(...products_part)
            facets.push(...facets_part)
        }

        const brands = facets.find(({name}) => name === 'brand')?.facetValues.reduce((obj, {id, name}) => ({...obj, [id]: name}), {}) || []

        return productsToFile('RP', products, brands)
    }

    let requestFilters = null
    let requestToken = null

    const parseAction = (() => {
        const {host} = window.location

        if (host === 'b2b.fh-as.dk') {
            console.log('F&H site')
            return startParse('FH')
        } else if (host === 'sales.rdg.dk') {
            console.log('Rosendahl sales')
            return startParse('RS')
        } else if (host === 'portal.rdg.dk') {
            console.log('Rosendahl portal')

            unsafeWindow.XMLHttpRequest.prototype.realSetRequestHeader = unsafeWindow.XMLHttpRequest.prototype.setRequestHeader;
            unsafeWindow.XMLHttpRequest.prototype.setRequestHeader = exportFunction(function(header, value) {
                if (header.toLowerCase() === 'authorization') {
                    requestToken = value;
                }
                unsafeWindow.XMLHttpRequest.prototype.realSetRequestHeader.call(this, ...arguments);
            }, unsafeWindow);

            unsafeWindow.XMLHttpRequest.prototype.realSend = unsafeWindow.XMLHttpRequest.prototype.send;
            unsafeWindow.XMLHttpRequest.prototype.send = exportFunction(function(data) {
                this.addEventListener("readystatechange", function() {
                    if (this.readyState === 4 && this.responseURL === 'https://b2b-api.rdg.dk/api/product/search') {
                        requestFilters = JSON.parse(data)
                    }
                }, false);
                unsafeWindow.XMLHttpRequest.prototype.realSend.call(this, data);
            }, unsafeWindow);

            return startParse('RP')
        }
    })()

    const button = $('<a href="#"/>')
    const buttonAction = (e) => {
        e.preventDefault()

        button.off('click')
        button.attr('disabled', 'disabled')

        parseAction()
    }
    const buttonProgress = (progress) => button.text(`Collecting products... [${Math.round(progress * 100 % 101)}%]`)

    button.css({
        position: 'fixed',
        bottom: 2,
        left: 2,
        display: 'inline-block',
        padding: 5,
        background: 'lightgray',
        color: 'black',
        zIndex: 999999,
        border: '1px solid black',
        borderRadius: 3
    })

    button.text('Update prices')

    button.on('click', buttonAction)

    setTimeout(() => {
        $('body').append(button)
    }, 1000)
})();
